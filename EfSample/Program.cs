﻿using EfSample.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EfSample
{
	class Program
	{
		static void Main(string[] args)
		{
			MyDbContext dbc = new MyDbContext();
			//dbc.Database.EnsureCreated();

			//Actor actor = new Actor
			//{
			//	Claims = new List<ActorClaim>
			//	{
			//		new ActorClaim { Name = "Name 1", Value = "Value 1" },
			//		new ActorClaim { Name = "Name 2", Value = "Value 2" }
			//	}
			//};

			//dbc.Actors.Add(actor);
			//dbc.SaveChanges();

			Actor[] actors = dbc.Actors
				.Include(e => e.Claims)
				.ToArray();

			actors[0].Claims[0].Value = "Update 1";
			dbc.SaveChanges();
		}
	}
}