﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EfSample.Entity
{
    public class Actor
    {
		public int Id { get; set; }

		public List<ActorClaim> Claims { get; set; }
	}
}
