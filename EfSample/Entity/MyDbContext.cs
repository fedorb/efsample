﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EfSample.Entity
{
    public class MyDbContext : DbContext
	{
		//public MyDbContext(DbContextOptions<MyDbContext> options = null)
		//	: base(options)
		//{
		//}

		public DbSet<Actor> Actors { get; set; }
		public DbSet<ActorClaim> Claims { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseNpgsql("Host=127.0.0.1;Database=test;Username=postgres;Password=postgres");

			base.OnConfiguring(optionsBuilder);
		}
	}
}
