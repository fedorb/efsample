﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EfSample.Entity
{
    public class ActorClaim
    {
		public int Id { get; set; }
		public int ActorRefId { get; set; }
		public string Name { get; set; }
		public string Value { get; set; }

		public Actor ActorRef { get; set; }
	}
}
